package com.example.animationexample;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void simpleButtonClicked(View view) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(findViewById(R.id.HelloTextView),
                "textSize", 100);
        animator.setDuration(1000);

        animator.start();
    }

//    public void fancyButtonClicked(View view) {
////        Animation bounceAnim =
//
//
//        // Create a new AnimatorSet.
//        AnimatorSet bouncer = new AnimatorSet();
//
//        // Specify when animations play relative to each other.
//        bouncer.play(bounceAnim).before(squashAnim1);
//        bouncer.play(squashAnim1).with(squashAnim2);
//        bouncer.play(squashAnim1).with(stretchAnim1);
//        bouncer.play(squashAnim1).with(stretchAnim2);
//        bouncer.play(bounceBackAnim).after(stretchAnim2);
//
//        // Create a fade animation
//        ValueAnimator fadeAnim = ObjectAnimator.ofFloat(newBall, "alpha", 1f, 0f);
//        fadeAnim.setDuration(250);
//
//        // Create second animator set that specifies to play the bouncer animation set
//        // before the fade animation.
//        AnimatorSet animatorSet = new AnimatorSet();
//        animatorSet.play(bouncer).before(fadeAnim);
//        animatorSet.start();
//    }
}
